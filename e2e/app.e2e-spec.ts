import { JptFrsPage } from './app.po';

describe('jpt-frs App', function() {
  let page: JptFrsPage;

  beforeEach(() => {
    page = new JptFrsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
